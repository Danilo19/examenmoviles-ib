package com.example.examen1

import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.evernote.android.state.State
import com.evernote.android.state.StateSaver
import com.google.android.material.textfield.TextInputEditText
import java.util.concurrent.ThreadLocalRandom

class MainGame : Fragment() {

    internal lateinit var round: TextView //round
    internal lateinit var gameScoreTextView: TextView
    internal lateinit var timeLeftTextView: TextView
    internal lateinit var goButton: Button //send
    ////////

    internal lateinit var numero1: TextView
    internal lateinit var numero2: TextView
    internal lateinit var simbolo: TextView
    internal lateinit var resultado: TextInputEditText
var total=0
    /////

    @State
    var score = 0

    @State
    var timeLeft = 10

    @State
    var gameStarted = false

    private var initialCountDown: Long = 10000
    private var countDownInterval: Long = 1000
    private  lateinit var countDownTimer: CountDownTimer


    private val args: MainGameArgs by navArgs()
//////////////////////////
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        StateSaver.restoreInstanceState(this, savedInstanceState)
        gameScoreTextView = view.findViewById(R.id.score)
        goButton = view.findViewById(R.id.go_button)
        timeLeftTextView = view.findViewById(R.id.time_left)

    numero1 = view.findViewById(R.id.numero1)
    numero2 = view.findViewById(R.id.numero2)
    simbolo = view.findViewById(R.id.simbolo)
    resultado = view.findViewById(R.id.resultadoTexto)



    gameScoreTextView.text = getString(R.string.score, score)
        timeLeftTextView.text = getString(R.string.time_left, timeLeft)

        round = view.findViewById(R.id.round)


    //val playerName = args.playerName
    var i=0
        round.text = getString(
            R.string.welcome_player,
            ""+i++)

    numero1.text=args.n1.toString()
    numero2.text=args.n2.toString()
    Log.d("nnnnnnnnn1",numero1.toString())
    //print("nnnnnnn1text"+numero1.text)

    if(args.a=="suma"){
       // countDownTimer.start()
    simbolo.text="+"

    total=numero1.text.toString().toInt() + numero2.text.toString().toInt()
        Log.d("TOOOOOTAAAAAAL",""+total)}
    if(args.a=="resta"){
        simbolo.text="-"
         total=numero1.text.toString().toInt() - numero2.text.toString().toInt()}
    if(args.a=="multiplicacion"){
        simbolo.text="x"
         total=numero1.text.toString().toInt() * numero2.text.toString().toInt()}
    if(args.a=="division"){
        simbolo.text="/"
        total=numero1.text.toString().toInt() / numero2.text.toString().toInt()}

//print(resultado.text.toString().toInt())


    goButton.setOnClickListener {
        if (total == resultado.text.toString().toInt()) {
            incrementScore(timeLeft.toLong())
            Log.d("RRRRRRRR",resultado.text.toString())
        }
    }

        //Log.d("RRRRRTEXT",resultado.text)

        if (gameStarted) {
            restoreGame()
            return
        }

        resetGame()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        StateSaver.saveInstanceState(this, outState)
        countDownTimer.start()//cancel***

    }

    override fun onCreateView(

        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_maingame, container, false)
    }


/////////////////

    fun sumar(n1:Int, n2:Int):Int{
        return n1+n2
    }

    fun restar(n1:Int, n2:Int):Int{
        return n1-n2
    }

    fun multiplicar(n1:Int, n2:Int):Int{
        return n1*n2
    }

    fun dividir(n1:Int, n2:Int):Double{
        return (n1/n2).toDouble()
    }



    fun generarAzar(range: Int): Int {
            return ThreadLocalRandom.current().nextInt(range)
        }


private fun incrementScore(tiempo:Long) {
    if (!gameStarted) {
        startGame()
    }
    else {
        if (tiempo < 10 && tiempo >= 8) {
            score += 100
            gameScoreTextView.text = getString(R.string.score, score)
            restoreGame()        }

        if (tiempo < 8 && tiempo >= 5) {
            score += 50
            gameScoreTextView.text = getString(R.string.score, score)
            restoreGame()        }

        if (tiempo < 5 && tiempo >= 0) {
            score += 10
            gameScoreTextView.text = getString(R.string.score, score)
            restoreGame()
        }
    }
}



//////////

private fun resetGame(){
    score = 0
    gameScoreTextView.text = getString(R.string.score, score)

    timeLeft = 10
    timeLeftTextView.text = getString(R.string.time_left, timeLeft)

    countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval) {
        override fun onFinish() {
            endGame()
        }
        override fun onTick(millisUntilFinished: Long) {
            timeLeft = millisUntilFinished.toInt() / 1000
            timeLeftTextView.text = getString(R.string.time_left, timeLeft)
        }
    }

    gameStarted = false

}
public fun startGame(){
    countDownTimer.start()
    gameStarted = true
}

private fun restoreGame(){
    gameScoreTextView.text = getString(R.string.score, score)

    countDownTimer = object : CountDownTimer(
        timeLeft * 1000L, countDownInterval) {
        override fun onFinish() {
            endGame()
        }
        override fun onTick(millisUntilFinished: Long) {
            timeLeft = millisUntilFinished.toInt() / 1000
            timeLeftTextView.text = getString(R.string.time_left, timeLeft)
        }
    }

    countDownTimer.start()
}

private fun endGame(){
    Toast.makeText(
        activity,
        getString(R.string.end_game, score),
        Toast.LENGTH_LONG).show()
    resetGame()
}
}
